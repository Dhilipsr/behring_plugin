<?php

namespace App\Http\Controllers;

use App\Resource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use LynX39\LaraPdfMerger\Facades\PdfMerger;

class PDFMergeController extends Controller
{
    public function pdfTools(Request $request)
    {	
        // return  $request->all();
    	$pdfArray = [];

    	$providers = $request->providerResources;
    	$providerResources = Resource::when($providers, function ($query, $providers) {
                    return $query->whereIn('id',$providers)->get();
                });

    	$providerFillableForm = $request->providerFillableForms;
    	$providerFillableForms = Resource::when($providerFillableForm, function ($query, $providerFillableForm) {
                    return $query->whereIn('id',$providerFillableForm)->get();
                });

    	$CPResource = $request->CPResources;
    	$CPResources = Resource::when($CPResource, function ($query, $CPResource) {
                    return $query->whereIn('id',$CPResource)->get();
                });

    	$CPSpanishResource = $request->CPSpanishResources;
    	$CPSpanishResources = Resource::when($CPSpanishResource, function ($query, $CPSpanishResource) {
                    return $query->whereIn('id',$CPSpanishResource)->get();
                });
    	
    	$VideoResource = $request->VideoResources;
    	$VideoResources = Resource::when($VideoResource, function ($query, $VideoResource) {
                    return $query->whereIn('id',$VideoResource)->get();
                });

    	foreach ($providerResources as $providerResource) {
    		if ($providerResource->can_print) {
    			$file = (json_decode($providerResource->pdf))[0]->download_link;
    			$pdfArray[] = $file;
    		}
    	}

    	foreach ($providerFillableForms as $providerFillableForm) {
    		if ($providerFillableForm->can_print) {
    			$file = (json_decode($providerFillableForm->pdf))[0]->download_link;
    			$pdfArray[] = $file;
    		}
    	}

    	foreach ($CPResources as $CPResource) {
    		if ($CPResource->can_print) {
    			$file = (json_decode($CPResource->pdf))[0]->download_link;
    			$pdfArray[] = $file;
    		}
    	}

    	foreach ($CPSpanishResources as $CPSpanishResource) {
    		if ($CPSpanishResource->can_print) {
    			$file = (json_decode($CPSpanishResource->pdf))[0]->download_link;
    			$pdfArray[] = $file;
    		}
    	}

    	foreach ($VideoResources as $VideoResource) {
    		if ($VideoResource->can_print) {
    			$file = (json_decode($VideoResource->pdf))[0]->download_link;
    			$pdfArray[] = $file;
    		}
    	}
        // return storage_path();
    	$pdfMerger = PDFMerger::init(); //Initialize the merger
    	foreach ( $pdfArray as $fileToMerge) {
            $storagePath  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
            $fileToMerge = str_replace('\\', '/', $fileToMerge);
            $fileToMerge = $storagePath.'public/'.$fileToMerge;
            $pdfMerger->addPDF($fileToMerge, 'all');
    		// $pdfMerger->addPDF('storage/'.$fileToMerge, 'all');
            // $pdfMerger->addPDF(public_path('storage/').$fileToMerge, 'all');
    	}
    	$pdfMerger->addPDF('/home/hizentraqsa/public_html/hizentra_plugin/storage/app/public/resources/September2021/Hizentra-Prescribing-Information.pdf','all');
    	if($request->providerResources)
    	{  
    	   //return  $request->providerResources;
    	   
    	     if( in_array('1', $request->providerResources) == true )
    	     {
    	       
    	         //$pdfMerger->addPDF('Pdf_pages.pdf','all');
    	     }
    	     else if(in_array('2',  $request->providerResources) == true)
             {
                 //$pdfMerger->addPDF('Pdf_pages.pdf','all');
             }
    	     else if( in_array('4', $request->providerResources) == true )
    	     {
    	       
    	         //$pdfMerger->addPDF('Pdf_pages.pdf','all');
    	     }
    	}
    	elseif($request->CPResources)
    	{
    	    //$pdfMerger->addPDF('/home/hizentraqsa/public_html/hizentra_plugin/storage/app/public/resources/September2021/Hizentra-Prescribing-Information.pdf','all');
    	    if(in_array('9', $request->CPResources) == true)
    	    {
    	         //$pdfMerger->addPDF('Pdf_pages.pdf','all');
    	    }
    	}
    	elseif($request->CPSpanishResources)
    	{
    	    if(in_array('9', $request->CPSpanishResources) == true)
    	    {
    	         //$pdfMerger->addPDF('Pdf_pages.pdf','all');
    	    }
    	}
    	
  
       
    	$pdfMerger->merge();
    	
    	$pdfMerger->save("file_name.pdf", "browser");
    }
}
