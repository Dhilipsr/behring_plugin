<?php

Route::get('/', 'PagesController@index')->name('home');
Route::get('/{clientSlug}', 'PagesController@orgIndex')->name('home.org');

Route::post('/view/tools/pdf', 'PDFMergeController@pdfTools')->name('pdfMerge');
// Route::post('/send/email', 'PagesController@sendEmails')->name('sendEmails');

Route::match(['get', 'post'],'/send/email', 'PagesController@sendEmails')->name('sendEmails');

Route::get('/Hizentra-PI','PagesController@viewPdf')->name('pi');

Route::get('command', function () {
	
	/* php artisan migrate */
    \Artisan::call('storage:link');
    dd("Done");
});


Route::get('cache-clear', function () {
	
	/* php artisan migrate */
    \Artisan::call('cache:clear');
    dd("Done");
});

Route::group(['prefix' => 'api'], function()
{
	Route::get('/get-order-details','PagesController@getOrder');
});


Route::group(['prefix' => 'admin/login'], function () {
    Voyager::routes();
});
