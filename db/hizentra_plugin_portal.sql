-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 07, 2022 at 09:23 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hizentra_plugin_portal`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `member_logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_logo_status` tinyint(3) UNSIGNED DEFAULT NULL,
  `sponser_status` tinyint(3) UNSIGNED DEFAULT NULL,
  `hamburger_status` tinyint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `name`, `slug`, `status`, `created_at`, `updated_at`, `member_logo`, `member_logo_status`, `sponser_status`, `hamburger_status`) VALUES
(312, 'Test Client', 'test-client-1', '1', '2021-10-12 15:14:00', '2021-10-12 15:17:41', NULL, 0, 0, 0),
(313, 'afx', 'afx', '1', '2021-11-05 10:56:52', '2021-11-05 10:56:52', NULL, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `clients_old`
--

CREATE TABLE `clients_old` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients_old`
--

INSERT INTO `clients_old` (`id`, `name`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Piyush', 'piyush', '1', '2020-05-27 10:33:06', '2020-05-27 10:33:06');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, '{}', 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, '{}', 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, '{}', 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 0, 1, 1, 1, 1, 1, '{}', 9),
(22, 4, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(23, 4, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|max:100\"}}', 2),
(24, 4, 'slug', 'text', 'Slug', 0, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"},\"validation\":{\"rule\":\"required|unique:clients\"}}', 3),
(25, 4, 'status', 'select_dropdown', 'Status', 0, 1, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"0\":\"No\",\"1\":\"Yes\"},\"validation\":{\"rule\":\"required\"}}', 4),
(26, 4, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(27, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(28, 5, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(29, 5, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 3),
(30, 5, 'spanish_title', 'text', 'Spanish Title', 0, 1, 1, 1, 1, 1, '{}', 4),
(31, 5, 'slug', 'text', 'Slug', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|unique:resources\"},\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}', 5),
(32, 5, 'menu_type', 'select_dropdown', 'Menu Type', 0, 1, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"1\":\"Provider\",\"2\":\"Caregiver and Paitients\",\"3\":\"Prescribing Information\",\"4\":\"Important Information\"},\"validation\":{\"rule\":\"required\"}}', 6),
(33, 5, 'category_type', 'select_dropdown', 'Category Type', 0, 1, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"1\":\"Resources\",\"2\":\"Fillable Forms\",\"3\":\"Videos\"},\"validation\":{\"rule\":\"required\"}}', 7),
(34, 5, 'pdf', 'file', 'Pdf', 0, 1, 1, 1, 1, 1, '{\"preserveFileUploadName\":\"yes\"}', 8),
(35, 5, 'spanish_pdf', 'file', 'Spanish Pdf', 0, 1, 1, 1, 1, 1, '{}', 9),
(36, 5, 'english_description', 'text_area', 'English Description', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 10),
(37, 5, 'spanish_description', 'text', 'Spanish Description', 0, 1, 1, 1, 1, 1, '{}', 11),
(38, 5, 'video_link', 'text', 'Video Link', 0, 1, 1, 1, 1, 1, '{}', 12),
(39, 5, 'cms_link', 'text', 'Cms Link', 0, 1, 1, 1, 1, 1, '{}', 13),
(40, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 14),
(41, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 15),
(42, 5, 'can_print', 'select_dropdown', 'Can Print', 0, 1, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"0\":\"No\",\"1\":\"Yes\"},\"validation\":{\"rule\":\"required\"}}', 2),
(43, 4, 'member_logo', 'image', 'Member Logo', 0, 1, 1, 1, 1, 1, '{}', 7),
(44, 4, 'member_logo_status', 'select_dropdown', 'Member Logo Status', 0, 1, 1, 1, 1, 1, '{\"default\":\"0\",\"options\":{\"0\":\"Inactive\",\"1\":\"Active\"}}', 8),
(45, 4, 'sponser_status', 'select_dropdown', 'Sponser Status', 0, 1, 1, 1, 1, 1, '{\"default\":\"0\",\"options\":{\"0\":\"Inactive\",\"1\":\"Active\"}}', 9),
(46, 4, 'hamburger_status', 'select_dropdown', 'Hamburger Status', 0, 1, 1, 1, 1, 1, '{\"default\":\"0\",\"options\":{\"0\":\"Inactive\",\"1\":\"Active\"}}', 10),
(47, 5, 'no_print_title', 'select_dropdown', 'No Print Title', 0, 1, 1, 1, 1, 1, '{\"default\":\"0\",\"options\":{\"0\":\"Hide\",\"1\":\"Show\"}}', 16),
(48, 5, 'separate_pi', 'select_dropdown', 'Does this PDF includes separate PI', 0, 1, 1, 1, 1, 1, '{\"default\":\"0\",\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}', 17),
(50, 5, 'pdf_with_pi', 'file', 'Pdf With Pi', 0, 1, 1, 1, 1, 1, '{\"preserveFileUploadName\":\"yes\"}', 18),
(51, 1, 'email_verified_at', 'timestamp', 'Email Verified At', 0, 1, 1, 1, 1, 1, '{}', 5);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2020-02-12 00:18:46', '2021-10-07 13:12:17'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-02-12 00:18:46', '2020-02-12 00:18:46'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2020-02-12 00:18:46', '2020-02-12 00:18:46'),
(4, 'clients', 'clients', 'Client', 'Clients', 'voyager-dot', 'App\\Client', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-02-12 01:06:52', '2020-08-31 17:09:42'),
(5, 'resources', 'resources', 'Resource', 'Resources', 'voyager-dot', 'App\\Resource', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-02-12 05:03:33', '2020-12-23 14:37:21');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-02-12 00:18:49', '2020-02-12 00:18:49');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2020-02-12 00:18:49', '2020-02-12 00:18:49', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2020-02-12 00:18:49', '2020-02-12 00:18:49', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2020-02-12 00:18:49', '2020-02-12 00:18:49', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2020-02-12 00:18:49', '2020-02-12 00:18:49', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2020-02-12 00:18:49', '2020-02-12 00:18:49', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 10, '2020-02-12 00:18:49', '2020-02-12 00:18:49', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 11, '2020-02-12 00:18:50', '2020-02-12 00:18:50', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 12, '2020-02-12 00:18:50', '2020-02-12 00:18:50', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 13, '2020-02-12 00:18:50', '2020-02-12 00:18:50', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2020-02-12 00:18:50', '2020-02-12 00:18:50', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 13, '2020-02-12 00:18:53', '2020-02-12 00:18:53', 'voyager.hooks', NULL),
(12, 1, 'Clients', '', '_self', 'voyager-dot', NULL, NULL, 15, '2020-02-12 01:06:52', '2020-02-12 01:06:52', 'voyager.clients.index', NULL),
(13, 1, 'Resources', '', '_self', 'voyager-dot', NULL, NULL, 16, '2020-02-12 05:03:34', '2020-02-12 05:03:34', 'voyager.resources.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-02-12 00:18:50', '2020-02-12 00:18:50'),
(2, 'browse_bread', NULL, '2020-02-12 00:18:50', '2020-02-12 00:18:50'),
(3, 'browse_database', NULL, '2020-02-12 00:18:50', '2020-02-12 00:18:50'),
(4, 'browse_media', NULL, '2020-02-12 00:18:50', '2020-02-12 00:18:50'),
(5, 'browse_compass', NULL, '2020-02-12 00:18:50', '2020-02-12 00:18:50'),
(6, 'browse_menus', 'menus', '2020-02-12 00:18:50', '2020-02-12 00:18:50'),
(7, 'read_menus', 'menus', '2020-02-12 00:18:50', '2020-02-12 00:18:50'),
(8, 'edit_menus', 'menus', '2020-02-12 00:18:50', '2020-02-12 00:18:50'),
(9, 'add_menus', 'menus', '2020-02-12 00:18:50', '2020-02-12 00:18:50'),
(10, 'delete_menus', 'menus', '2020-02-12 00:18:50', '2020-02-12 00:18:50'),
(11, 'browse_roles', 'roles', '2020-02-12 00:18:50', '2020-02-12 00:18:50'),
(12, 'read_roles', 'roles', '2020-02-12 00:18:50', '2020-02-12 00:18:50'),
(13, 'edit_roles', 'roles', '2020-02-12 00:18:50', '2020-02-12 00:18:50'),
(14, 'add_roles', 'roles', '2020-02-12 00:18:51', '2020-02-12 00:18:51'),
(15, 'delete_roles', 'roles', '2020-02-12 00:18:51', '2020-02-12 00:18:51'),
(16, 'browse_users', 'users', '2020-02-12 00:18:51', '2020-02-12 00:18:51'),
(17, 'read_users', 'users', '2020-02-12 00:18:51', '2020-02-12 00:18:51'),
(18, 'edit_users', 'users', '2020-02-12 00:18:51', '2020-02-12 00:18:51'),
(19, 'add_users', 'users', '2020-02-12 00:18:51', '2020-02-12 00:18:51'),
(20, 'delete_users', 'users', '2020-02-12 00:18:51', '2020-02-12 00:18:51'),
(21, 'browse_settings', 'settings', '2020-02-12 00:18:51', '2020-02-12 00:18:51'),
(22, 'read_settings', 'settings', '2020-02-12 00:18:51', '2020-02-12 00:18:51'),
(23, 'edit_settings', 'settings', '2020-02-12 00:18:51', '2020-02-12 00:18:51'),
(24, 'add_settings', 'settings', '2020-02-12 00:18:51', '2020-02-12 00:18:51'),
(25, 'delete_settings', 'settings', '2020-02-12 00:18:51', '2020-02-12 00:18:51'),
(26, 'browse_hooks', NULL, '2020-02-12 00:18:53', '2020-02-12 00:18:53'),
(27, 'browse_clients', 'clients', '2020-02-12 01:06:52', '2020-02-12 01:06:52'),
(28, 'read_clients', 'clients', '2020-02-12 01:06:52', '2020-02-12 01:06:52'),
(29, 'edit_clients', 'clients', '2020-02-12 01:06:52', '2020-02-12 01:06:52'),
(30, 'add_clients', 'clients', '2020-02-12 01:06:52', '2020-02-12 01:06:52'),
(31, 'delete_clients', 'clients', '2020-02-12 01:06:52', '2020-02-12 01:06:52'),
(32, 'browse_resources', 'resources', '2020-02-12 05:03:34', '2020-02-12 05:03:34'),
(33, 'read_resources', 'resources', '2020-02-12 05:03:34', '2020-02-12 05:03:34'),
(34, 'edit_resources', 'resources', '2020-02-12 05:03:34', '2020-02-12 05:03:34'),
(35, 'add_resources', 'resources', '2020-02-12 05:03:34', '2020-02-12 05:03:34'),
(36, 'delete_resources', 'resources', '2020-02-12 05:03:34', '2020-02-12 05:03:34');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1);

-- --------------------------------------------------------

--
-- Table structure for table `resources`
--

CREATE TABLE `resources` (
  `id` int(10) UNSIGNED NOT NULL,
  `can_print` tinyint(3) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spanish_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pdf` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spanish_pdf` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `english_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spanish_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cms_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `no_print_title` tinyint(3) UNSIGNED DEFAULT NULL,
  `separate_pi` tinyint(3) UNSIGNED DEFAULT NULL,
  `pdf_with_pi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `resources`
--

INSERT INTO `resources` (`id`, `can_print`, `title`, `spanish_title`, `slug`, `menu_type`, `category_type`, `pdf`, `spanish_pdf`, `english_description`, `spanish_description`, `video_link`, `cms_link`, `created_at`, `updated_at`, `no_print_title`, `separate_pi`, `pdf_with_pi`) VALUES
(3, 1, '<div>SKYTROFA<sup>®</sup>Specialty Pharmacy Flashcard</div>', NULL, 'div-skytrofa-sup-r-sup-specialty-pharmacy-flashcard-div', '1', '1', '[{\"download_link\":\"resources\\/September2021\\/Optimizing the Transition to Hizentra for CIDP Patients.pdf\",\"original_name\":\"Optimizing the Transition to Hizentra for CIDP Patients.pdf\"}]', '[]', 'Contains important information about how to contact the Ascendis Pharma \r\nSignature Access Program™ (A•S•A•P) to get the Auto-Injector for patients.', NULL, NULL, 'https://cc-oge.online/hiz-cidp-cslb-master/optimizing-the-transition-to-hizentra-for-cidp-patients-6117', '2020-02-29 02:29:00', '2022-02-03 05:11:28', 0, 0, '[]'),
(4, 1, 'Statement of Medical Necessity form', NULL, 'statement-of-medical-necessity-form', '1', '2', '[{\"download_link\":\"resources\\/September2021\\/Benefits Investigation Request.pdf\",\"original_name\":\"Benefits Investigation Request.pdf\"}]', '[]', 'This fillable verification form will be right at your fingertips when you\'re \r\nready to prescribe SKYTROFA.', NULL, NULL, 'https://cc-oge.online/hiz-cidp-cslb-master/hizentra-connect-resource-center—benefits-investigation-request-prescr-6120', '2020-02-29 02:33:00', '2022-02-03 05:18:19', 0, 0, '[]'),
(6, 1, 'Free Trial Program Request Form', NULL, 'free-trial-program-request-form', '1', '8', '[{\"download_link\":\"resources\\/September2021\\/Free Trial Program Request Form1.pdf\",\"original_name\":\"Free Trial Program Request Form.pdf\"}]', '[]', 'Helps eligible patients receive a trial of Hizentra', NULL, NULL, 'https://cc-oge.online/hiz-cidp-cslb-master/free-trial-program-request-form-6121', '2020-02-29 02:34:00', '2021-09-24 20:02:21', 0, 0, '[]'),
(8, 1, 'Gains Beyond Growth Patient Brochure', NULL, 'gains-beyond-growth-patient-brochure', '2', '1', '[{\"download_link\":\"resources\\/September2021\\/My Life My Way With Hizentra.pdf\",\"original_name\":\"My Life My Way With Hizentra.pdf\"}]', '[]', 'Give your patients the information they need to know about SKYTROFA, the \r\nSKYTROFA Auto-Injector and the Ascendis Signature Access Program (A•S•A•P)\r\npersonalized patient support.', NULL, NULL, 'https://cc-oge.online/hiz-cidp-cslb-master/my-life,-my-way-with-hizentra-6148', '2020-02-29 02:36:00', '2022-02-03 05:20:21', 0, 0, '[]'),
(9, 1, 'Ascendis Signature Access Program (A•S•A•P) Patient Brochure', NULL, 'ascendis-signature-access-program-a-s-a-p-patient-brochure', '2', '1', '[{\"download_link\":\"resources\\/September2021\\/Is Self-infused Ig Right for Your CIDP.pdf\",\"original_name\":\"Is Self-infused Ig Right for Your CIDP.pdf\"}]', '[]', 'Your patients will find all the information they need about the services and support \r\nprovided by the A•S•A•P program, including insurance and financial support, \r\nauto-injector delivery and training, and how to get additional information and \r\nsupport if they need it.', NULL, NULL, 'https://cc-oge.online/hiz-cidp-cslb-master/is-self-infused-ig-right-for-your-cidp-6144', '2020-02-29 02:38:00', '2022-02-03 05:21:54', 0, 0, '[]'),
(16, 1, '<div class=\"adndsaf\" >SKYTROFA<sup>®</sup> Dosing Options</div>', NULL, 'div-class-adndsaf-skytrofa-sup-r-sup-dosing-options-div', '1', '1', '[{\"download_link\":\"resources\\/October2021\\/Managing Common Infusion Issues.pdf\",\"original_name\":\"Managing Common Infusion Issues.pdf\"}]', '[]', 'You’ll find straightforward dosing recommendations and a simple calculation \r\nfor individualized dosing along with recommendations to make switching to \r\nonce-weekly SKYTROFA easy.', NULL, NULL, 'https://cc-oge.online/hiz-cidp-cslb-master/managing-common-infusion-issues-6257', '2020-02-29 02:29:00', '2022-02-03 05:10:05', 0, 0, '[]'),
(17, 1, '<div>Get Ready to Start e-Prescribing SKYTROFA<sup>®</sup></div>', NULL, 'div-get-ready-to-start-e-prescribing-skytrofa-sup-r-sup-div', '1', '1', '[]', '[]', 'Learn how to make sure SKYTROFA is set up properly in your EHR to make \r\ne-prescribing SKYTROFA as seamless as possible.', NULL, NULL, NULL, '2022-02-03 02:26:00', '2022-02-03 05:17:06', 0, 0, '[]');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2020-02-12 00:18:50', '2020-02-12 00:18:50'),
(2, 'user', 'Normal User', '2020-02-12 00:18:50', '2020-02-12 00:18:50');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Quick Support Access', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Quick Support Access Admin Panel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(3, 1, 'QSA Admin', 'plugin@admin.com', NULL, '$2b$10$cvNGarzoSSc4qAv.PJV.vOcOwxk4chqwURQcCFQmH6l9aLRAE.G4.', 'yQipAk1Bu9BAWVhGodiutglbjJFxlxhdqXCJNLLItKE9h69tEuUj6uX5ADxl', '{\"locale\":\"en\"}', '2020-02-12 00:39:18', '2020-12-29 15:28:42');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients_old`
--
ALTER TABLE `clients_old`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `resources`
--
ALTER TABLE `resources`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=314;

--
-- AUTO_INCREMENT for table `clients_old`
--
ALTER TABLE `clients_old`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `resources`
--
ALTER TABLE `resources`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
