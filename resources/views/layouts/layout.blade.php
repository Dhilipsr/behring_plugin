<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset = "UTF-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="{{asset('Hizentra_favicon-01-01.jpg')}}">
    <link rel="icon" href="{{asset('Hizentra_favicon-01-01.jpg')}}" type="image/gif" sizes="16x16">
    <!-- Place favicon.ico in the root directory -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    
    {{-- Page level css --}}
    @yield('css')
    {{-- Page level css --}}
    
   <!-- Google Tag Manager -->

<!-- End Google Tag Manager -->
</head> 
   
   
 

<body>
  <!-- Google Tag Manager (noscript) -->

 
<!-- End Google Tag Manager (noscript) -->

    <div class="frame ">
        @include('partials.header') 
        
        {{-- content starts here --}}
        @yield('content')
        {{-- content ends here --}}
        
        @include('partials.flash')
        
        @include('partials.footer')
    </div>
    {{-- Modal Starts here --}}
    @include('partials.modal')
    {{-- Modal Ends Here --}}

    <script src="{{asset('js/vendor/modernizr-3.5.0.min.js')}}"></script>
    <script src="{{asset('js/vendor/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <script src="{{asset('js/plugins.js')}}"></script>
    
    {{-- Page level js --}}
    @yield('js')
    {{-- Page level js --}}
    <script src="{{asset('js/main.js')}}"></script>
</body>

</html>